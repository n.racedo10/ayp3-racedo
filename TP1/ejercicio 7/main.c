#include <stdio.h>
#include <stdlib.h>

int main()
{
    int opcion = 0;
    while (opcion != 4){
        printf("\nSeleccione un personaje para leer su frase:\n");
        printf("1. Albert Einstein\n");
        printf("2. Jorge Luis Borges\n");
        printf("3. Marcelo Gallardo\n");
        printf("4. Salir\n");

        printf("Ingrese opcion: ");
        scanf("%i", &opcion);
        switch (opcion){
            case 1:
                printf("La mente es como un paracaidas. Solo funciona si la tenemos abierta.\n");
                break;
            case 2:
                printf("La duda es uno de los nombres de la inteligencia.\n");
                break;
            case 3:
                printf("Que la gente crea, porque tiene con que creer.\n");
                break;
        }
    }
    return 0;
}
