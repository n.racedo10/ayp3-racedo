#include <stdio.h>
#include <stdlib.h>

int main()
{
    int lista[6] = {12, 22, 31, 10, 99, 83};
    int minimo = lista[0];

    printf("Lista: ");
    for(int i = 0; i < 6; i++){
        printf("%i ", lista[i]);
        if (lista[i] < minimo){
            minimo = lista[i];
        }
    }
    printf("\nMinimo: %i", minimo);
    return 0;
}
