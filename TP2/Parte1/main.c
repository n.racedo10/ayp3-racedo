#include <stdio.h>

typedef struct {
    char marca[20];
    char tipo[16];
    int modelo;
    char color[16];
} Auto;

int main() {
    Auto miAuto = {"Volkswagen", "Fox", 2015, "Rojo"};
    printf("Auto: %s, %s | Modelo: %d | Color: %s", miAuto.marca, miAuto.tipo, miAuto.modelo, miAuto.color);
    return 0;
}
