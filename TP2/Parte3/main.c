#include <stdio.h>
#include<stdlib.h>

typedef struct {
    char *nombre;
    char *apellido;
    int edad;
    char *hijo1;
    char *hijo2;
} Persona;

Persona construirPersona(char nombre[20], char apellido[16], int edad, char hijo1[20], char hijo2[20]){
    Persona laPersona;
    Persona *plaPersona;

    plaPersona = &laPersona;

    plaPersona->nombre = (char *) malloc(20 * sizeof(char));
    plaPersona->apellido = (char *) malloc(16 * sizeof(char));
    plaPersona->hijo1 = (char *) malloc(40 * sizeof(char));
    plaPersona->hijo2 = (char *) malloc(40 * sizeof(char));

    plaPersona->nombre = nombre;
    plaPersona->apellido = apellido;
    plaPersona->edad = edad;
    plaPersona->hijo1 = hijo1;
    plaPersona->hijo2 = hijo2;

    return laPersona;
}

int main() {
    Persona pedro = construirPersona("Pedro", "Marquez", 45, "Juan, Marquez", "Sofía, Marquez");
    printf("Persona: %s, %s | Edad: %d \n Hijos: %s y %s", pedro.nombre, pedro.apellido, pedro.edad, pedro.hijo1, pedro.hijo2);
    return 0;
}
