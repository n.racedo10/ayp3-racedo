# TP2
## Parte 1

Para la primer parte creo que no tuve inconvenientes, lo resolví así:

```c
#include <stdio.h>

typedef struct {
    char marca[20];
    char tipo[16];
    int modelo;
    char color[16];
} Auto;

int main() {
    Auto miAuto = {"Volkswagen", "Fox", 2015, "Rojo"};
    printf("Auto: %s, %s | Modelo: %d | Color: %s", miAuto.marca, miAuto.tipo, miAuto.modelo, miAuto.color);
    return 0;
}
```

## Parte 2

Al querér modularizar me surgieron algunos problemas, primero intenté esto:

```c
#include <stdio.h>

typedef struct {
    char marca[20];
    char tipo[16];
    int modelo;
    char color[16];
} Auto;

Auto construirAuto(char marca[20], char tipo[16], int modelo, char color[16]){
    Auto elAuto = {marca, tipo, modelo, color};
    return elAuto;
}

int main() {
    Auto miAuto = construirAuto("Volkswagen", "Fox", 2015, "Rojo");
    printf("Auto: %s, %s | Modelo: %d | Color: %s", miAuto.marca, miAuto.tipo, miAuto.modelo, miAuto.color);
    return 0;
}
```

Lo cuál no funciona:

> Output: 
Auto: �,  | Modelo: 0 | Color:  

Investigando y viendo videos, llegué a la siguiente solución:

```c
#include <stdio.h>
#include<stdlib.h>

typedef struct {
    char *marca; //Defino que la estructura va a contener punteros a tipo char
    char *tipo;
    int modelo;
    char *color;
} Auto;

Auto construirAuto(char marca[20], char tipo[16], int modelo, char color[16]){
    Auto elAuto; // Variable elAuto de tipo struct Auto
    Auto *pAuto; // Puntero de tipo struct Auto

    pAuto = &elAuto; // Le asigno al puntero pAuto la dirección de memoria de la variable elAuto

    pAuto->marca = (char *) malloc(20 * sizeof(char)); // Reservo un espacio en memoria para cada uno de los arreglos.
    pAuto->tipo = (char *) malloc(16 * sizeof(char));
    pAuto->color = (char *) malloc(16 * sizeof(char));

    pAuto->marca = marca; // Le asigno al puntero marca el valor que se pasa por parametro para marca
    pAuto->tipo = tipo;
    pAuto->modelo = modelo;
    pAuto->color = color;

    return elAuto; // retorno la variable elAuto
}

int main() {
    Auto miAuto = construirAuto("Volkswagen", "Fox", 2015, "Rojo");
    printf("Auto: %s, %s | Modelo: %d | Color: %s", miAuto.marca, miAuto.tipo, miAuto.modelo, miAuto.color);
    return 0;
}
```

> Output:
Auto: Volkswagen, Fox | Modelo: 2015 | Color: Rojo

Entendí que para que la estructura pueda almacenar un arreglo de caracteres es suficiente y necesario almacenar el puntero a dicho elemento.
Luego hay que reservar un espacio en memoria para ese dato utilizando malloc().
Una vez que tenemos reservado ese espacio en memoria, se le puede asignar el valor que se le pasa por parametro a la función que realiza la modularización. [construirAuto()]


## Parte 3

Propuse una estructura similar, aunque no entendí si la idea es que los hijos sean otra estructura de tipo persona. En ese caso no sabría como resolverlo.

```c
#include <stdio.h>
#include<stdlib.h>

typedef struct {
    char *nombre;
    char *apellido;
    int edad;
    char *hijo1;
    char *hijo2;
} Persona;

Persona construirPersona(char nombre[20], char apellido[16], int edad, char hijo1[20], char hijo2[20]){
    Persona laPersona;
    Persona *plaPersona;

    plaPersona = &laPersona;

    plaPersona->nombre = (char *) malloc(20 * sizeof(char));
    plaPersona->apellido = (char *) malloc(16 * sizeof(char));
    plaPersona->hijo1 = (char *) malloc(40 * sizeof(char));
    plaPersona->hijo2 = (char *) malloc(40 * sizeof(char));

    plaPersona->nombre = nombre;
    plaPersona->apellido = apellido;
    plaPersona->edad = edad;
    plaPersona->hijo1 = hijo1;
    plaPersona->hijo2 = hijo2;

    return laPersona;
}

int main() {
  Persona pedro = construirPersona("Pedro", "Marquez", 45, "Juan, Marquez", "Sofía, Marquez");
  printf("Persona: %s, %s | Edad: %d \n Hijos: %s y %s", pedro.nombre, pedro.apellido, pedro.edad, pedro.hijo1, pedro.hijo2);
  return 0;
}
```