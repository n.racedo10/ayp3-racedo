#include <stdio.h>
#include<stdlib.h>

typedef struct {
    char *marca; //Defino que la estructura va a contener punteros a tipo char
    char *tipo;
    int modelo;
    char *color;
} Auto;

Auto construirAuto(char marca[20], char tipo[16], int modelo, char color[16]){
    Auto elAuto; // Variable elAuto de tipo struct Auto
    Auto *pAuto; // Puntero de tipo struct Auto

    pAuto = &elAuto; // Le asigno al puntero pAuto la dirección de memoria de la variable elAuto

    pAuto->marca = (char *) malloc(20 * sizeof(char)); // Reservo un espacio en memoria para cada uno de los arreglos.
    pAuto->tipo = (char *) malloc(16 * sizeof(char));
    pAuto->color = (char *) malloc(16 * sizeof(char));

    pAuto->marca = marca; // Le asigno al puntero marca el valor que se pasa por parametro para marca
    pAuto->tipo = tipo;
    pAuto->modelo = modelo;
    pAuto->color = color;

    return elAuto; // retorno la variable elAuto
}

int main() {
    Auto miAuto = construirAuto("Volkswagen", "Fox", 2015, "Rojo");
    printf("Auto: %s, %s | Modelo: %d | Color: %s", miAuto.marca, miAuto.tipo, miAuto.modelo, miAuto.color);
    return 0;
}