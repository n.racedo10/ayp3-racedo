#include <stdio.h>

typedef struct estructuraNodo {
    int valor;
    struct estructuraNodo *proximo;
} Nodo;

Nodo *agregarElemento(Nodo *lista, int valor) {
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (lista == NULL) {
        lista = nodoNuevo;
    } else {
        Nodo *cursor = lista;
        while (cursor->proximo != NULL) {
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }
    return lista;
}

Nodo *agregarElementoOrdenado(Nodo *lista, int valor) {
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (lista == NULL) {
        lista = nodoNuevo;
    } else {
        Nodo *cursor = lista;
        while (cursor->valor < valor && cursor->proximo != NULL) {
            cursor = cursor->proximo;
        }
        if (cursor->proximo == NULL){
            // Estoy al final de la lista
            if (cursor->valor < valor){
                cursor->proximo = nodoNuevo;
            }else{
                cursor->proximo = nodoNuevo;
                nodoNuevo->valor = cursor->valor;
                cursor->valor = valor;

                //if()
                //cursor->proximo = NULL;
                if (lista->proximo == NULL){
                    lista = nodoNuevo;
                }
            }

        }else if(cursor->valor > valor && lista == cursor){
            nodoNuevo->proximo = cursor;
            lista = nodoNuevo;
        }else {
            Nodo *nodo_proximo = cursor->proximo;
            cursor->proximo = nodoNuevo;
            nodoNuevo->proximo = nodo_proximo;
            nodoNuevo->valor = cursor->valor;
            cursor->valor = valor;
        }
    }
    return lista;
}

Nodo *crearLista() {
    Nodo *lista = malloc(sizeof(Nodo));
    lista = NULL;
    return lista;
}

int obtenerLargo(Nodo *lista){

    int largo;
    if (lista == NULL) {
        largo = 0;
    }else{
        largo = 1;
        Nodo *cursor = lista;

        while (cursor->proximo != NULL) {
            cursor = cursor->proximo;
            largo++;
        }
    }

    return largo;
}

int obtenerElemento(Nodo *lista, int i){

    int largo = obtenerLargo(lista);
    int elemento;
    int j = 0;

    if (i > largo-1){
        elemento = -1;
    }else{
        Nodo *cursor = lista;

        while ( j<i ) {
            cursor = cursor->proximo;
            j++;
        }

        elemento = cursor->valor;
    }

    return elemento;
}

void eliminarElemento(Nodo *lista, int i){

    int largo = obtenerLargo(lista);
    int j = 0;

    if (i > largo-1){
        printf("No hay elemento en esa posicion.");
    }else{
        Nodo *cursor = lista;

        while (j<i-1) {
            cursor = cursor->proximo;
            j++;
        }

        if (i == largo - 1) {
            cursor->proximo = NULL;
        }else{
            if (j==i-1){
                cursor->proximo = cursor->proximo->proximo;
            }else {
                cursor->valor = cursor->proximo->valor;
                cursor->proximo = cursor->proximo->proximo;
            }
        }
    }
}

void imprimirLista(Nodo *lista){
    if (lista == NULL) {
        printf("[ Empty ]");
    }else {
        Nodo *cursor = lista;
        printf("Lista: [ ");
        while (cursor->proximo != NULL) {
            printf("%d, ", cursor->valor);
            cursor = cursor->proximo;
        }
        printf("%d", cursor->valor);
        printf(" ]");
    }
}

int main() {

    Nodo *lista = crearLista();
    lista = agregarElemento(lista, 2);
    lista = agregarElemento(lista, 3);
    lista = agregarElemento(lista, 4);
    lista = agregarElemento(lista, 5);

    lista = agregarElementoOrdenado(lista, 11);
    lista = agregarElementoOrdenado(lista, 10);
    lista = agregarElementoOrdenado(lista, 24);
    lista = agregarElementoOrdenado(lista, 18);
    lista = agregarElementoOrdenado(lista, 1);
    lista = agregarElementoOrdenado(lista, 12);
    lista = agregarElementoOrdenado(lista, 30);
    lista = agregarElementoOrdenado(lista, 0);
    lista = agregarElementoOrdenado(lista, 20);
    lista = agregarElementoOrdenado(lista, 99);

    //eliminarElemento(lista, 5);

    int largo = obtenerLargo(lista);
    int obtenerPos2 = obtenerElemento(lista, 2);

    printf("%d \n", lista->valor);
    printf("%d \n", lista->proximo->valor);
    printf("%d \n", lista->proximo->proximo->valor);
    printf("Largo de la lista: %d \n", largo);
    printf("Elemento en i = 2: %d \n", obtenerPos2);

    imprimirLista(lista);

    return 0;
}